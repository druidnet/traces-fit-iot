#!/usr/bin/env nix-shell
#!nix-shell --pure -i python3 -p python3Packages.psutil python3Packages.pandas python3Packages.zstandard
import re
import sqlite3
import sys
import tarfile
import zstandard
import os
import pandas as pd
import io
from collections import defaultdict

if len(sys.argv) != 4:
    print(f"Usage: {sys.argv[0]} <experiment.tar.gz> <server_log.csv> <output_database.db3>")
    exit(-1)

tarname = sys.argv[1]
serveroutputname = sys.argv[2]
dbname = sys.argv[3]

# Regex to find the timestamp
prog = re.compile(r"([^\n])([0-9]{10}\.[0-9]{6};)")

#
# Nodes Data
#

dataframes = {
    "info" : (pd.DataFrame(columns=["Measurement", "Timestamp", "Node", "Message"]), []),
    "neighbor_stats" : None,
    "output": (pd.DataFrame(columns=["Measurement", "Timestamp", "Node", "Output Stdout"]), []),
    "rpl_stats" : None,
    "rpl_stats_dodag" : None,
    "rpl_stats_instance" : None,
    "rpl_stats_parents" : None,
    "rpl_status" : None,
    "stats" : None,
    "temperature" : None,
    "udp" : None
}

numeric_columns = ["rx packets", "rx bytes", "tx packets", "tx multicast packets", "tx bytes", "tx succeeded", "tx errors", "success"]
categorical_columns = ["Node", "layer", "Measurement", "payload size", "success", "destination address", "destination port"]

nodes_dataframes = defaultdict(lambda: dataframes)

cnx = sqlite3.connect(dbname)

with open(tarname, 'rb') as fh:
    dctx = zstandard.ZstdDecompressor()
    reader = dctx.stream_reader(fh)
    leftover_chunk = None
    while True:
        print("New chunk")
        chunk = ""
        if leftover_chunk:
            chunk = leftover_chunk
            leftover_chunk = None

        chunk += reader.read(1000*1000*500).decode("utf-8")
        
        if not chunk:
            break

        # Repair broken lines (split)
        chunk = re.sub(prog, r"\g<1>\r\g<2>", chunk)
        lines = chunk.splitlines()        

        for line in lines[:-1]:
            try:
                (timestamp, node, data) = line.split(";")
                splitted = data.split(',')
                key = splitted[0]
                if key in dataframes.keys(): # Formatted Data
                    if nodes_dataframes[node][key] is None: # The first line is the column name
                        print(f"Creating new column for {key} and {node} with {splitted[1:]}")
                        nodes_dataframes[node][key] = (pd.DataFrame(columns=["Measurement", "Timestamp", "Node"] + splitted[1:]), [])
                    else:
                        # Handle broken output
                        if key == "info":
                            nodes_dataframes[node][key][1].append([key, timestamp, node] + [",".join(splitted[1:])])
                        else:
                            nodes_dataframes[node][key][1].append([key, timestamp, node] + splitted[1:])
                else: # Stdout Data
                    nodes_dataframes[node]["output"][1].append(["output", timestamp, node] + [data])
            except ValueError as e:
                print(e)
                print("Problem with the parsing of this line: ")
                print(line)
                continue
        leftover_chunk = lines[-1]

        new_nodes_dataframes = {}
        for node, node_dataframe in nodes_dataframes.items():
            new_dataframes = {}
            for key, value in node_dataframe.items():
                if value is None:
                    continue
                (df, data_to_write) = value
                df = df.append(pd.DataFrame(data_to_write, columns=df.columns))
                for column in numeric_columns:
                    if column in df.columns:
                        try:
                            df[column] = pd.to_numeric(df[column])
                        except ValueError as E:
                            print(E)
                            print(df[column])
                for column in categorical_columns:
                    if column in df.columns:
                        df[column] = df[column].astype('category')
                df["Timestamp"] = pd.to_datetime(df["Timestamp"], unit="s")
                df.to_sql(name=key, con=cnx, if_exists='append')
                new_dataframes[key] = (pd.DataFrame(columns=df.columns), [])
            new_nodes_dataframes[node] = new_dataframes
        nodes_dataframes = new_nodes_dataframes
        cnx.commit()