#!/usr/bin/env bash

SITE=grenoble
MODELS=m3
N=10
DURATION=10
EXPERIMENT_SCRIPT="script.sh"

# Launch the experiment and obtain its ID
EXPID=$(iotlab-experiment submit -d $DURATION -l $N,archi=m3:at86rf231+site=$SITE,gnrc_networking.elf -l 1,archi=m3:at86rf231+site=$SITE,gnrc_border_router.elf | jq ".id")

# Wait for the experiment to began
iotlab-experiment wait -i $EXPID

NODES_LIST=$(iotlab-experiment get -i $EXPID -p | jq '.firmwareassociations[] | select(.firmwarename | contains("border_router") | not) | .nodes | map( split(".")[0] | split("-")[1] ) | join("+")')
BORDER_LIST=$(iotlab-experiment get -i $EXPID -p | jq '.firmwareassociations[] | select(.firmwarename | contains("border_router")) | .nodes | map( split(".")[0] ) | join(" ")')

ENVIRONMENT=${EXPID}.env
echo "export NODES_LIST=$SITE,$MODELS,${NODES_LIST}
export BORDER_LIST=${BORDER_LIST}" > ${ENVIRONMENT}

scp $ENVIRONMENT $SITE.iot-lab.info: 

# Launch the serial aggregation for the nodes
iotlab-experiment script -i $EXPID --run $SITE,script=${EXPERIMENT_SCRIPT}

# Cleanup the env
rm ${ENVIRONMENT}
