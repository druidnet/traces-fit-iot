with (import (builtins.fetchTarball { url="https://github.com/NixOS/nixpkgs/archive/refs/tags/20.09.tar.gz"; sha256 = "1wg61h4gndm3vcprdcg7rc4s1v3jkm5xd7lw8r2f67w502y94gcy"; }) { });
with lib;

let
  firmware_names = ["gnrc_border_router" "gnrc_networking"];
  callPackage = lib.callPackageWith (pkgs // packages);
  packages =
    rec {
      fetchSWH = callPackage ./pkgs/fetchSWH { };
      RIOT = callPackage ./pkgs/RIOT { };
      iotlabcli = python3Packages.callPackage ./pkgs/iotlabcli { };
      inherit pkgs;
    } // (listToAttrs (map (name: { inherit name; value = callPackage ./pkgs/firmware_builder { firmware_name = name; }; }) firmware_names));
in
mkShell {
  RIOTBASE = packages.RIOT;
  buildInputs = with packages; [
    iotlabcli
    jq
    openssh
    python3
    ansible
    zstd
  ];
  shellHook = ''
    mkdir -p build/
  '' + concatMapStrings (name: "install " + (packages.${name}.out + ("/" + name + ".elf")) + " ./build/; ") firmware_names;
}
