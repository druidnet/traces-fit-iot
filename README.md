# Prerequisites

Install Nix: [https://nixos.org/download.html](https://nixos.org/download.html)

# How to build

Run the following command:

```
nix-shell
```

If everything goes fine, firmwares have been built in the `build/` directory. If you prefer, you can also in a shell where you can build the examples of in the `src` directory *manually* by launching `make`, e.g:
```
[user@machine:~/traces-fit-iot/firmwares]$ nix-shell

[nix-shell:~/traces-fit-iot/firmwares]$ cd src/Udp_Gnrc_Example/
[nix-shell:~/traces-fit-iot/firmwares/src/Udp_Gnrc_Example]$ make
Building application "gnrc_networking_mac" for "iotlab-m3" with MCU "stm32".

"make" -C /nix/store/qa5jndp3xwqpakqvxwxmgabh3hwifzyb-RIOT-2021.01-branch/boards/iotlab-m3
"make" -C /nix/store/qa5jndp3xwqpakqvxwxmgabh3hwifzyb-RIOT-2021.01-branch/boards/common/iotlab
"make" -C /nix/store/qa5jndp3xwqpakqvxwxmgabh3hwifzyb-RIOT-2021.01-branch/core

[ . . . ]

"make" -C /nix/store/qa5jndp3xwqpakqvxwxmgabh3hwifzyb-RIOT-2021.01-branch/sys/trickle
"make" -C /nix/store/qa5jndp3xwqpakqvxwxmgabh3hwifzyb-RIOT-2021.01-branch/sys/tsrb
"make" -C /nix/store/qa5jndp3xwqpakqvxwxmgabh3hwifzyb-RIOT-2021.01-branch/sys/xtimer
   text    data     bss     dec     hex filename
 102340     184   20100  122624   1df00 /home/user/traces-fit-iot/firmwares/src/Udp_Gnrc_Example/bin/iotlab-m3/gnrc_networking_mac.elf

[nix-shell:~/traces-fit-iot/firmwares/src/Udp_Gnrc_Example]$ ls -alh /home/user/traces-fit-iot/firmwares/src/Udp_Gnrc_Example/bin/iotlab-m3/gnrc_networking_mac.elf
-rwxr-xr-x 1 user users 3,1M mars  19 11:25 /home/user/traces-fit-iot/firmwares/src/Udp_Gnrc_Example/bin/iotlab-m3/gnrc_networking_mac.elf

[nix-shell:~/traces-fit-iot/firmwares/src/Udp_Gnrc_Example]$
```

# Troubleshooting

## Hash Mismatch

Due to the fact the RIOT-OS repository fetch dependencies at runtime, the RIOT package use a fixed-output hash derivation. If some dependency changes, you will get a hash mismatch, e.g.:

```
hash mismatch in fixed-output derivation '/nix/store/2vgfjaha38nrh4q6766nkql258vw972f-RIOT-2021.01-branch':
  wanted: sha256:1y6s4bm65srpywvw4r7zk86acx1kr9xjnhdyqyrg77b0fq533iww
  got:    sha256:1h20kzmj8j6hfkyqysm9ss68ij42drdvs1c7vga324j33gwiwlhd
error: build of '/nix/store/1w3p51lsijcfcpl33qjqjmsvlifvmp2b-RIOT-2021.01-branch.drv' failed
```

You will have to replace the offending hash in the `pkgs/RIOT/default.nix` file, e.g.:

```
---  outputHash = "1y6s4bm65srpywvw4r7zk86acx1kr9xjnhdyqyrg77b0fq533iww";
+++  outputHash = "1h20kzmj8j6hfkyqysm9ss68ij42drdvs1c7vga324j33gwiwlhd";

```

# Artifacts and automatic build

Please do not commit binaries (the `.elf` and `.bin`) to the repository, as they are built (or will be built in the near future) automatically by the continuous integration.
